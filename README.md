# sa-token-demo-cross

#### 介绍

Sa-Token 处理跨域示例

配套文章： https://juejin.cn/post/7247376558367981627

``` js
├── sa-token-demo-cross-header-server     // [示例] Sa-Token 跨域测试 - Header 参数版，后端接口
├── sa-token-demo-cross-header-h5         // [示例] Sa-Token 跨域测试 - Header 参数版，h5 页面（jquery请求）
├── sa-token-demo-cross-header-vue3       // [示例] Sa-Token 跨域测试 - Header 参数版，vue3 页面
├── sa-token-demo-cross-cookie-server     // [示例] Sa-Token 跨域测试 - 第三方 Cookie 版，后端接口
├── sa-token-demo-cross-cookie-h5         // [示例] Sa-Token 跨域测试 - 第三方 Cookie 版，h5 页面（jquery请求）
├── sa-token-demo-cross-cookie-vue3       // [示例] Sa-Token 跨域测试 - 第三方 Cookie 版，vue3 页面
```